let typing = false
let imgIndex = 0

window.onload = () => {
    startSplashScreen()
    addSplashButtonEvents()
    addNavMenuEvents()
    handleBlurability()
    handleOverlay()
    addProjectCards()
    //loadServiceWorker()
    log('👀 Feel free to poke around! 👀')
}

function addNavMenuEvents() {
    qs('#nav-logo').onclick = () => {
        let pageToHide = qs('.page.fade-in-right')
        let homePage = qs('#home-page')
        let navbar = qs('#header')
        homePage.className = 'page fade-in-left active'
        pageToHide.className = 'page fade-out-right'
        navbar.className = 'fade-out-top'
    }

    let navButtons = qsa('.nav-btn')
    for (let i = 0; i < navButtons.length; i++) {
        navButtons[i].onclick = (e) => {
            let pageToHide = qs('.page.fade-in-right')
            let pageToShow = qs(`#${e.target.dataset.pageId}-page`)

            selectNavbarButton(e.target)
            pageToHide.className = 'page fade-out-left'
            pageToShow.className = 'page fade-in-right active'
        }
    }
}

function addSplashButtonEvents() {
    let splashButtons = qsa('.splash-btn')
    for (let i = 0; i < splashButtons.length; i++) {
        splashButtons[i].onclick = (e) => {
            let pageToShow = qs(`#${e.target.dataset.pageId}-page`)
            let homePage = qs('#home-page')
            let navbar = qs('#header')

            selectNavbarButton(qs(`#${e.target.dataset.pageId}-btn`))

            homePage.className = 'page fade-out-left'
            pageToShow.className = 'page fade-in-right active'
            navbar.className = 'fade-in-top'

        }
    }
}

function selectNavbarButton(btn) {
    if (!btn.classList.contains('active')) {
        qs('.nav-btn.active').classList.remove('active')
        btn.classList.add('active')
    }
}

function handleBlurability() {
    let testElement = document.createElement('div')
    if (testElement.style.backdropFilter == undefined) {
        log('Browser does not support backdrop blur.. Maybe one day.')
        let pages = qsa('.page')
        for (let i = 0; i < pages.length; i++) {
            pages[i].style.paddingTop = '0'
            pages[i].style.marginTop = '112px'
        }
    }
}

function handleOverlay() {
    document.querySelector('#overlay').onclick = (e) => {
        if(e.target.id === 'overlay') {
            overlay.classList.add('hidden')
            overlay.innerHTML = ''
        }
    }

}

async function submitContactForm() {
    let name= qs('#contact-name-input').value
    let email= qs('#contact-email-input').value
    let subject= qs('#contact-subject-input').value
    let message= qs('#contact-message-input').value

    let form = `name=${name}&email=${email}&subject=${subject}&message=${message}`

    let res = await sendRequest('POST', 'https://haydenives.com/php/contact.php', 
                                [['Content-Type', 'application/x-www-form-urlencoded']],
                                form)

    res = JSON.parse(res.response)
    if(res.status === 0) {
        alert(res.body)
    } else {
        log(res.body)
        qs('#contact-form-container').className = 'hidden'
    }
}


function addProjectCards() {
    let container = qs('.projects-container')
    for(let i = 0; i < projects.length; i++) {
        /*
        <div class="project-header">
            <span class="project-title">${projects[i].title}</span>
        </div>
        <div class="project-card-content">
            <div class="project-media-container">
                <a class="project-img" style="background-image: url(${projects[i].images[0]})"></a>
            </div>
            <div class="project-info-container">
                ${projects[i].description}
            </div>
        </div>
        <div class="project-footer">
            ${projects[i].footer}
        </div>
        */
        let element = document.createElement('div')
        element.className = 'project-card'
        element.innerHTML = `<div class="project-header"><span class="project-title">${projects[i].title}</span></div><div class="project-card-content"><div class="project-media-container"><a class="project-img" style="background-image: url(${projects[i].images[0]})"></a></div><div class="project-info-container">${projects[i].description}</div></div><div class="project-footer">${projects[i].footer}</div>`
        element.querySelector('.project-img').onclick = () => {showSlideShow(projects[i].title, projects[i].description, projects[i].images)}
        
        container.appendChild(element)
    }
}

async function startSplashScreen() {
    let splash = qs('#splash-typing')
    typing = true;
    await write(splash, tasks.WRITE, '$ ')
    await write(splash, tasks.WRITE, 'haydenIves', 6)
    await write(splash, tasks.WRITE, '\n$ ')
    updateDescriptor(splash)
}

async function updateDescriptor(element) {
    typing = true
    let descriptor = descriptors[Math.floor(Math.random()*descriptors.length)]

    await write(element, tasks.WRITE, descriptor, 6)

    setTimeout(async () => {
        await write(element, tasks.DELETE, descriptor.length)
        setTimeout(() => {
            typing = false
        }, 500)
    }, 1000)
}

function showSlideShow(title, desc, images) {
    let container = document.createElement('div')
    container.id = 'slideshow-container'

    imgIndex = 0

    /*
    <div id="slideshow-title">
    ${title}
    </div>
    <div id="slideshow-pic-container">
        <div id="slideshow-pic-overlay">
            <div class="slideshow-overlay-btn" id="slideshow-overlay-left">
                <div class="overlay-btn"><</div>
            </div>
            <div class="slideshow-overlay-btn" id="slideshow-overlay-right">
                <div class="overlay-btn">></div>
            </div>
        </div>
    </div>
    <div id="slideshow-desc">
    ${desc}
    </div>
    <span id="slideshow-page-counter">1 of ${images.length}</span>
    */
    container.innerHTML = `<div id="slideshow-title">${title}</div><div id="slideshow-pic-container"><div id="slideshow-pic-overlay"><div class="slideshow-overlay-btn" id="slideshow-overlay-left"><div class="overlay-btn"><</div></div><div class="slideshow-overlay-btn" id="slideshow-overlay-right"><div class="overlay-btn">></div></div></div></div><div id="slideshow-desc">${desc}</div><span id="slideshow-page-counter">1 of ${images.length}</span>`
    
    container.querySelector('#slideshow-overlay-left').onclick = () => {
        if(imgIndex <= 0) {
            imgIndex = images.length
        }
        qsa(`[data-index='${imgIndex%images.length}']`)[0].className = 'slideshow-pic hidden'
        imgIndex--
        qsa(`[data-index='${imgIndex%images.length}']`)[0].className = 'slideshow-pic'
        qs('#slideshow-page-counter').innerText = (imgIndex%images.length + 1) + ' of ' + images.length

    }
    container.querySelector('#slideshow-overlay-right').onclick = () => {
        qsa(`[data-index='${imgIndex%images.length}']`)[0].className = 'slideshow-pic hidden'
        imgIndex++
        qsa(`[data-index='${imgIndex%images.length}']`)[0].className = 'slideshow-pic'
        qs('#slideshow-page-counter').innerText = (imgIndex%images.length + 1) + ' of ' + images.length
    }

    let img;
    let picContainer = container.querySelector('#slideshow-pic-container')
    for(let i = 0; i < images.length; i++) {
        img = document.createElement('img')
        img.className = (i === 0) ? 'slideshow-pic' : 'slideshow-pic hidden'
        img.src = images[i]
        img.dataset.index = i;
        picContainer.appendChild(img)
    }
    
    let overlay = qs('#overlay')
    overlay.appendChild(container)
    overlay.classList.remove('hidden')
}

function loadServiceWorker() {
    if ('serviceWorker' in navigator) {
          navigator.serviceWorker.register('/sw.js').then(function(registration) {
            // Registration was successful
            console.log('ServiceWorker registration successful with scope: ', registration.scope);
          }, function(err) {
            // registration failed :(
            console.log('ServiceWorker registration failed: ', err);
            alert('error', err)
          });
      }
}

function write(element, task, msg, typoRate) {
    if (element == undefined || task == undefined) {
        log('Typewritting needs element and task')
    }

    if (task === tasks.WRITE) {
        return new Promise((resolve) => {
            typewriter(resolve, element, msg, typoRate)
        })
    } else if (task === tasks.DELETE) {
        return new Promise((resolve) => {
            backspace(resolve, element, Number(msg))
        })
    } else {
        log(`Unknown task type: ${task}`)
        return
    }
}

function typewriter(resolve, element, msg, typoRate, remove) {
    let char = msg[0]
    if (char == undefined) {
        resolve()
        return
    }

    typoRate = (typoRate != undefined || typoRate > 0) ? typoRate : 0
    remove = (remove != undefined) ? remove : false

    setTimeout(() => {
        if (remove) {
            element.innerText = element.innerText.substr(0, element.innerText.length - 1)
            typewriter(resolve, element, msg, typoRate, false)
        } else if (Math.floor(Math.random() * typoRate) == 1) {
            element.innerText += Math.random().toString(36)[3]
            typewriter(resolve, element, msg, typoRate, true)
        } else {
            element.innerText += char
            typewriter(resolve, element, msg.slice(1), typoRate, false)
        }
    }, (Math.floor(Math.random() * 100)) + 50)
}

function backspace(resolve, element, length) {
    if (length === 0) {
        resolve()
        return
    }

    setTimeout(() => {
        element.innerText = element.innerText.substr(0, element.innerText.length - 1)
        backspace(resolve, element, --length)
    }, (Math.floor(Math.random() * 50)) + 50)
}

window.setInterval(() => {
    if(!typing && qs('#home-page').classList.contains('active')) {
        updateDescriptor(qs('#splash-typing'))
    }
}, 1000)

// ************************************************
// function: sendRequest
//
// method: HTML Method. Ex. GET, POST, PUT...
// url: URL to send request to
// headers: array of header key-value pairs [[key1, val1], [key2, val2],...]
// data: payload if using methods such as POST 
// 
// return type: Promise
// returns: response for request
// 
// description: function that uses xhr to make requests
// As requests should be asynchronous this method returns
// a promise. Expectation is to call this function with
// 'await' from inside an async function
// ************************************************
var sendRequest = function (method, url, headers, data) {
    // Promise is used for an async call that will
    // be waiting on the node server
    return new Promise(resolve => {

        // request will be an xhr
        var req = new XMLHttpRequest();

        // Add event listener for readystate changing to 'done'
        req.onreadystatechange = function () {

            // 4 means the request is done
            if (this.readyState == 4) {

                // I am simply sending the whole response back to
                // the function that called sendRequest()
                resolve(req);
            }
        };

        // after opening request, set headers. Default to json
        // for content-type if not specified
        req.open(method, url, true);

        if(headers !== undefined) {
            for(let i = 0; i < headers.length; i++) {
                if(headers[i].length !== 2) {
                    error('Header key-val mismatch')
                }
                req.setRequestHeader(headers[i][0], headers[i][1])
            }
        } else {
            req.setRequestHeader("Content-Type", "application/json");
        }

        // As this function is used for HTTP methods with and
        // without payloads, I only send the 'data' attribute
        // if it is defined. Otherwise, send no payload
        if (data != undefined) {
            if(typeof(data) === 'object') {
                data = JSON.stringify(data)
            }
            req.send(data)
        } else {
            req.send()
        }
    });
}
